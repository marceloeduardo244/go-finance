package api

import (
	"database/sql"
	"net/http"

	"github.com/gin-gonic/gin"
	db "gitlab.com/marceloeduardo244/go-finance/db/sqlc"
	enums "gitlab.com/marceloeduardo244/go-finance/enums"
	"gitlab.com/marceloeduardo244/go-finance/util"
)

type createCategoryRequest struct {
	UserID      int32  `json:"user_id" binding:"required"`
	Title       string `json:"title" binding:"required"`
	Type        string `json:"type" binding:"required"`
	Description string `json:"description" binding:"required"`
}

func (server *Server) createCategory(ctx *gin.Context) {
	util.Logger.Info("Executed func createCategory!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, enums.Unathorized.EnumIndex())
		return
	}
	var req createCategoryRequest
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func createCategory", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	arg := db.CreateCategoryParams{
		UserID:      req.UserID,
		Title:       req.Title,
		Type:        req.Type,
		Description: req.Description,
	}

	category, err := server.store.CreateCategory(ctx, arg)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func createCategory", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
	}

	ctx.JSON(http.StatusOK, category)
}

type getCategoryRequest struct {
	ID int32 `uri:"id" binding:"required"`
}

func (server *Server) getCategory(ctx *gin.Context) {
	util.Logger.Info("Executed func getCategory!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, enums.Unathorized.EnumIndex())
		return
	}
	var req getCategoryRequest
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getCategory", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	category, err := server.store.GetCategory(ctx, req.ID)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getCategory", err)
		if err == sql.ErrNoRows {
			ctx.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, category)
}

type deleteCategoryRequest struct {
	ID int32 `uri:"id" binding:"required"`
}

func (server *Server) deleteCategory(ctx *gin.Context) {
	util.Logger.Info("Executed func deleteCategory!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, enums.Unathorized.EnumIndex())
		return
	}
	var req deleteCategoryRequest
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func deleteCategory", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	err = server.store.DeleteCategories(ctx, req.ID)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func deleteCategory", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, true)
}

type updateCategoryRequest struct {
	ID          int32  `json:"id" binding:"required"`
	Title       string `json:"title"`
	Description string `json:"description"`
}

func (server *Server) updateCategory(ctx *gin.Context) {
	util.Logger.Info("Executed func updateCategory!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, enums.Unathorized.EnumIndex())
		return
	}
	var req updateCategoryRequest
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func updateCategory", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	arg := db.UpdateCategoriesParams{
		ID:          req.ID,
		Title:       req.Title,
		Description: req.Description,
	}

	category, err := server.store.UpdateCategories(ctx, arg)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func updateCategory", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
	}

	ctx.JSON(http.StatusOK, category)
}

type getCategoriesRequest struct {
	UserID int32  `uri:"user_id" binding:"required"`
	Type   string `uri:"type" binding:"required"`
}

func (server *Server) getCategories(ctx *gin.Context) {
	util.Logger.Info("Executed func getCategories!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, enums.Unathorized.EnumIndex())
		return
	}
	var req getCategoriesRequest
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getCategories", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	arg := db.GetCategoriesParams{
		UserID: req.UserID,
		Type:   req.Type,
	}

	categories, err := server.store.GetCategories(ctx, arg)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getCategories", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
	}

	ctx.JSON(http.StatusOK, categories)
}
