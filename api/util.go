package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/marceloeduardo244/go-finance/util"
)

func (server *Server) isAlive(ctx *gin.Context) {
	util.Logger.Info("Executed func isAlive!")
	ctx.JSON(http.StatusOK, "The server is Alive! GoPackGo!")
}
