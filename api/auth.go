package api

import (
	"bytes"
	"crypto/sha512"
	"database/sql"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	enums "gitlab.com/marceloeduardo244/go-finance/enums"
	"gitlab.com/marceloeduardo244/go-finance/util"
	"golang.org/x/crypto/bcrypt"
)

type loginRequest struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type Claims struct {
	Username string `json:"username"`
	jwt.RegisteredClaims
}

func (server *Server) login(ctx *gin.Context) {
	util.Logger.Info("Executed func getAccounts!")
	var req loginRequest
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func login", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	user, err := server.store.GetUser(ctx, req.Username)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func login", err)
		if err == sql.ErrNoRows {
			ctx.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	hashedInput := sha512.Sum512_256([]byte(req.Password))
	trimmedHash := bytes.Trim(hashedInput[:], "\x00")
	preparedPassword := string(trimmedHash)
	plainTextInBytes := []byte(preparedPassword)
	hashTextInBytes := []byte(user.Password)
	err = bcrypt.CompareHashAndPassword(hashTextInBytes, plainTextInBytes)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func login", err)
		ctx.JSON(http.StatusUnauthorized, enums.Unathorized.EnumIndex())
		return
	}

	expirationTimeParam, _ := time.ParseDuration(os.Getenv("TOKEN_TIME"))
	expirationTime := time.Now().Add(expirationTimeParam * time.Minute)

	claims := &Claims{
		Username: req.Username,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
		},
	}

	generatedToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	var jwtSignedKey = []byte(os.Getenv("SECRET_KEY"))
	generatedTokenToString, err := generatedToken.SignedString(jwtSignedKey)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func login", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, generatedTokenToString)
}
