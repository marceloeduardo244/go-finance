package api

import (
	"bytes"
	"crypto/sha512"
	"database/sql"
	"net/http"

	"github.com/gin-gonic/gin"
	db "gitlab.com/marceloeduardo244/go-finance/db/sqlc"
	enums "gitlab.com/marceloeduardo244/go-finance/enums"
	"gitlab.com/marceloeduardo244/go-finance/util"

	"golang.org/x/crypto/bcrypt"
)

type createUserRequest struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
	Email    string `json:"email" binding:"required"`
}

func (server *Server) createUser(ctx *gin.Context) {
	util.Logger.Info("Executed func createUser!")
	var req createUserRequest
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func createUser", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	hashedInput := sha512.Sum512_256([]byte(req.Password))
	trimmedHash := bytes.Trim(hashedInput[:], "\x00")
	preparedPassword := string(trimmedHash)
	passwordHashInBytes, err := bcrypt.GenerateFromPassword([]byte(preparedPassword), bcrypt.DefaultCost)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func createUser", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
	}
	var passwordHashed = string(passwordHashInBytes)
	arg := db.CreateUserParams{
		Username: req.Username,
		Password: passwordHashed,
		Email:    req.Email,
	}

	user, err := server.store.CreateUser(ctx, arg)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func createUser", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
	}

	ctx.JSON(http.StatusOK, user)
}

type getUserRequest struct {
	Username string `uri:"username" binding:"required"`
}

func (server *Server) getUser(ctx *gin.Context) {
	util.Logger.Info("Executed func getUser!")
	var req getUserRequest
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getUser", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	user, err := server.store.GetUser(ctx, req.Username)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getUser", err)
		if err == sql.ErrNoRows {
			ctx.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, user)
}

type getUserByIdRequest struct {
	ID int32 `uri:"id" binding:"required"`
}

func (server *Server) getUserById(ctx *gin.Context) {
	util.Logger.Info("Executed func getUserById!")
	var req getUserByIdRequest
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getUserById", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	user, err := server.store.GetUserById(ctx, req.ID)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getUserById", err)
		if err == sql.ErrNoRows {
			ctx.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, user)
}

type deleteUserByIdRequest struct {
	ID int32 `uri:"id" binding:"required"`
}

func (server *Server) deleteUserById(ctx *gin.Context) {
	util.Logger.Info("Executed func deleteUserById!")
	var req deleteUserByIdRequest
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func deleteUserById", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	err1 := server.store.DeleteUser(ctx, req.ID)
	if err1 != nil {
		util.Logger.Sugar().Errorf("Error on func deleteUserById", err1)
		if err1 == sql.ErrNoRows {
			ctx.JSON(http.StatusNotFound, errorResponse(err1))
			return
		}
		ctx.JSON(http.StatusInternalServerError, errorResponse(err1))
		return
	}

	ctx.JSON(http.StatusOK, true)
}

type updateUserRequest struct {
	ID       int32  `json:"id" binding:"required"`
	Username string `json:"username"`
	Email    string `json:"email"`
}

func (server *Server) updateUser(ctx *gin.Context) {
	util.Logger.Info("Executed func updateUser!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		util.Logger.Sugar().Errorf("Error on func updateUser", errOnValiteToken)
		ctx.JSON(http.StatusUnauthorized, enums.Unathorized.EnumIndex())
		return
	}
	var req updateUserRequest
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func updateUser", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	arg := db.UpdateUserParams{
		ID:       req.ID,
		Username: req.Username,
		Email:    req.Email,
	}

	category, err := server.store.UpdateUser(ctx, arg)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func updateUser", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
	}

	ctx.JSON(http.StatusOK, category)
}
