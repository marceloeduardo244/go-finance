package api

import (
	"github.com/gin-gonic/gin"
	ginprometheus "github.com/zsais/go-gin-prometheus"
	db "gitlab.com/marceloeduardo244/go-finance/db/sqlc"
	dbTool "gitlab.com/marceloeduardo244/go-finance/db/tools"

	cors "github.com/rs/cors/wrapper/gin"
)

type Server struct {
	store  *db.SQLStore
	router *gin.Engine
}

func NewServer(store *db.SQLStore) *Server {
	dbTool.RunMigrationOnInitialization()

	server := &Server{store: store}
	router := gin.Default()
	router.Use(cors.AllowAll())

	p := ginprometheus.NewPrometheus("gin")
	p.Use(router)

	router.GET("/server/isAlive", server.isAlive)

	router.POST("/user", server.createUser)
	router.GET("/user/:username", server.getUser)
	router.GET("/user/id/:id", server.getUserById)
	router.PUT("/user/:id", server.updateUser)
	router.DELETE("/user/id/:id", server.deleteUserById)

	router.POST("/category", server.createCategory)
	router.GET("/category/id/:id", server.getCategory)
	router.GET("/category/all/:user_id/:type", server.getCategories)
	router.DELETE("/category/:id", server.deleteCategory)
	router.PUT("/category/:id", server.updateCategory)

	router.POST("/account", server.createAccount)
	router.GET("/account/id/:id", server.getAccount)
	router.GET("/account/all/:user_id/:type", server.getAccounts)
	router.GET("/account/graph/:user_id/:type", server.getAccountGraph)
	router.GET("/account/reports/:user_id/:type", server.getAccountReports)
	router.DELETE("/account/:id", server.deleteAccount)
	router.PUT("/account/:id", server.updateAccount)

	router.POST("/login", server.login)

	server.router = router
	return server
}

func (server *Server) Start(address string) error {
	return server.router.Run(address)
}

func errorResponse(err error) gin.H {
	return gin.H{"api has error:": err.Error()}
}
