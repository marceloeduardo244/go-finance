package api

import (
	"database/sql"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	db "gitlab.com/marceloeduardo244/go-finance/db/sqlc"
	"gitlab.com/marceloeduardo244/go-finance/util"
)

type createAccountRequest struct {
	UserID      int32     `json:"user_id" binding:"required"`
	CategoryID  int32     `json:"category_id" binding:"required"`
	Title       string    `json:"title" binding:"required"`
	Type        string    `json:"type" binding:"required"`
	Description string    `json:"description" binding:"required"`
	Value       int32     `json:"value" binding:"required"`
	Date        time.Time `json:"date" binding:"required"`
}

func (server *Server) createAccount(ctx *gin.Context) {
	util.Logger.Info("Executed func createAccount!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, errorResponse(errOnValiteToken))
		return
	}
	var req createAccountRequest
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func createAccount", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	var categoryId = req.CategoryID
	var accountType = req.Type

	category, err := server.store.GetCategory(ctx, categoryId)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func createAccount", err)
		ctx.JSON(http.StatusNotFound, errorResponse(err))
	}

	var categoryTypeIsDifferentOfAccountType = category.Type != accountType
	if categoryTypeIsDifferentOfAccountType {
		ctx.JSON(http.StatusBadRequest, "Account type is different of Category type")
	} else {
		arg := db.CreateAccountParams{
			UserID:      req.UserID,
			CategoryID:  categoryId,
			Title:       req.Title,
			Type:        accountType,
			Description: req.Description,
			Value:       req.Value,
			Date:        req.Date,
		}

		account, err := server.store.CreateAccount(ctx, arg)
		if err != nil {
			util.Logger.Sugar().Errorf("Error on func createAccount", err)
			ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		}

		ctx.JSON(http.StatusOK, account)
	}
}

type getAccountRequest struct {
	ID int32 `uri:"id" binding:"required"`
}

func (server *Server) getAccount(ctx *gin.Context) {
	util.Logger.Info("Executed func getAccount!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, errorResponse(errOnValiteToken))
		return
	}
	var req getAccountRequest
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getAccount", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	account, err := server.store.GetAccount(ctx, req.ID)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getAccount", err)
		if err == sql.ErrNoRows {
			ctx.JSON(http.StatusNotFound, errorResponse(err))
			return
		}
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, account)
}

type getAccountGraphRequest struct {
	UserID int32  `uri:"user_id" binding:"required"`
	Type   string `uri:"type" binding:"required"`
}

func (server *Server) getAccountGraph(ctx *gin.Context) {
	util.Logger.Info("Executed func getAccountGraph!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, errorResponse(errOnValiteToken))
		return
	}
	var req getAccountGraphRequest
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getAccountGraph", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	arg := db.GetAccountsGraphParams{
		UserID: req.UserID,
		Type:   req.Type,
	}

	countGraph, err := server.store.GetAccountsGraph(ctx, arg)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getAccountGraph", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, countGraph)
}

type getAccountReportsRequest struct {
	UserID int32  `uri:"user_id" binding:"required"`
	Type   string `uri:"type" binding:"required"`
}

func (server *Server) getAccountReports(ctx *gin.Context) {
	util.Logger.Info("Executed func getAccountReports!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, errorResponse(errOnValiteToken))
		return
	}
	var req getAccountReportsRequest
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getAccountReports", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	arg := db.GetAccountsReportsParams{
		UserID: req.UserID,
		Type:   req.Type,
	}

	sumReports, err := server.store.GetAccountsReports(ctx, arg)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getAccountReports", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, sumReports)
}

type deleteAccountRequest struct {
	ID int32 `uri:"id" binding:"required"`
}

func (server *Server) deleteAccount(ctx *gin.Context) {
	util.Logger.Info("Executed func deleteAccount!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, errorResponse(errOnValiteToken))
		return
	}
	var req deleteAccountRequest
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func deleteAccount", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	err = server.store.DeleteAccount(ctx, req.ID)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func deleteAccount", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, true)
}

type updateAccountRequest struct {
	ID          int32  `json:"id" binding:"required"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Value       int32  `json:"value"`
}

func (server *Server) updateAccount(ctx *gin.Context) {
	util.Logger.Info("Executed func updateAccount!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, errorResponse(errOnValiteToken))
		return
	}
	var req updateAccountRequest
	err := ctx.ShouldBindJSON(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func updateAccount", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
	}

	arg := db.UpdateAccountParams{
		ID:          req.ID,
		Title:       req.Title,
		Description: req.Description,
		Value:       req.Value,
	}

	account, err := server.store.UpdateAccount(ctx, arg)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func updateAccount", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
	}

	ctx.JSON(http.StatusOK, account)
}

type getAccountsRequest struct {
	UserID int32  `uri:"user_id" binding:"required"`
	Type   string `uri:"type" binding:"required"`
}

func (server *Server) getAccounts(ctx *gin.Context) {
	util.Logger.Info("Executed func getAccounts!")
	errOnValiteToken := util.GetTokenInHeaderAndVerify(ctx)
	if errOnValiteToken != nil {
		ctx.JSON(http.StatusUnauthorized, errorResponse(errOnValiteToken))
		return
	}
	var req getAccountsRequest
	err := ctx.ShouldBindUri(&req)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getAccounts", err)
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	arg := db.GetAccountsParams{
		UserID: req.UserID,
		Type:   req.Type,
	}

	accounts, err := server.store.GetAccounts(ctx, arg)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func getAccounts", err)
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, accounts)
}
