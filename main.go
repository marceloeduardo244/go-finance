package main

import (
	"database/sql"
	"fmt"
	"os"
	"time"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/marceloeduardo244/go-finance/api"
	db "gitlab.com/marceloeduardo244/go-finance/db/sqlc"
	"gitlab.com/marceloeduardo244/go-finance/util"
)

func main() {
	util.InitializeLogger()
	err := godotenv.Load()
	if err != nil {
		util.Logger.Sugar().Errorf("Error loading .env file", err)
		util.Logger.Sugar().Errorf("If you are in Docker ou Kubernetes you dont need the .env file, don't worry.", err)
	}

	time.Sleep(10 * time.Second)
	fmt.Println("Waiting database check and initialization")

	dbDriver := os.Getenv("DB_DRIVER")
	dbSource := os.Getenv("DB_SOURCE")
	serverAddress := os.Getenv("SERVER_ADDRESS")

	conn, err := sql.Open(dbDriver, dbSource)
	if err != nil {
		util.Logger.Sugar().Errorf("cannot connect to db: ", err)
	}

	store := db.NewStore(conn)
	server := api.NewServer(store)

	err = server.Start(serverAddress)
	if err != nil {
		util.Logger.Sugar().Errorf("cannot start api: ", err)
	}
	util.Logger.Info("Application Go-finance Backend started with success!")
}
