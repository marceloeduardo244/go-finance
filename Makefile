createDb:
	createdb --username=postgres --owner=postgres go_finance

postgres:
	docker run --name postgres -p 5432:5432 -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=go_finance -d postgres:14-alpine

pgadmin:
	docker run --name pgadmin -p 80:80 -e PGADMIN_DEFAULT_EMAIL=marcelo@gmail.com -e PGADMIN_DEFAULT_PASSWORD=SuperSecret -d dpage/pgadmin4

migrationUp:
	 C:\Users\marcelo.oliveira\scoop\shims\migrate.exe -path db/migration -database "postgresql://postgres:postgres@localhost:5432/go_finance?sslmode=disable" -verbose up

migrationDown:
	 C:\Users\marcelo.oliveira\scoop\shims\migrate.exe -path db/migration -database "postgresql://postgres:postgres@localhost:5432/go_finance?sslmode=disable" -verbose down

test:
	go test -v -cover ./test/sqlc
	
server:
	go run main.go

sqlc-gen:
	docker run --rm -v "C:\GitEstudo\go-finance:/src" -w /src kjconroy/sqlc generate

.PHONY: createDb postgres migrationUp migrationDown