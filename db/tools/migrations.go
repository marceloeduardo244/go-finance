package db

import (
	"database/sql"
	"os"

	migrate "github.com/golang-migrate/migrate/v4"
	postgres "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
	"gitlab.com/marceloeduardo244/go-finance/util"
)

func OpenConnection() (*sql.DB, error) {
	dbDriver := os.Getenv("DB_DRIVER")
	dbSource := os.Getenv("DB_SOURCE")
	conn, err := sql.Open(dbDriver, dbSource)
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func OpenConnection", err)
		panic(err)
	}
	err = conn.Ping()
	if err != nil {
		util.Logger.Sugar().Errorf("Error on func OpenConnection", err)
		panic(err)
	}
	return conn, err
}

func RunMigrationOnInitialization() {
	conn, _ := OpenConnection()
	driver, _ := postgres.WithInstance(conn, &postgres.Config{})
	m, err := migrate.NewWithDatabaseInstance(
		"file://./db/migration",
		"postgres", driver)
	if err != nil {
		util.Logger.Sugar().Errorf("cannot execute migrations: ", err)
		panic(err)
	}
	m.Up()
	util.Logger.Info("Migrations executed with success!")
}
