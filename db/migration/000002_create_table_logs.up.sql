CREATE TABLE "logs" (
  "id" serial PRIMARY KEY NOT NULL,
  "action" varchar NOT NULL,
  "created_at" timestamptz NOT NULL DEFAULT (now())
);