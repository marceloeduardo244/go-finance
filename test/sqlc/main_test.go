package db

import (
	"database/sql"
	"os"
	"testing"

	_ "github.com/lib/pq"
	db "gitlab.com/marceloeduardo244/go-finance/db/sqlc"
	util "gitlab.com/marceloeduardo244/go-finance/util"
)

var testQueries *db.Queries

func TestMain(m *testing.M) {
	dbDriver := os.Getenv("DB_DRIVER")
	dbSource := os.Getenv("DB_SOURCE")
	conn, err := sql.Open(dbDriver, dbSource)

	if err != nil {
		util.Logger.Sugar().Errorf("Error on func TestMain", err)
	}

	testQueries = db.New(conn)
	os.Exit(m.Run())
}
