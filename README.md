![image](https://interestedvideos.com/wp-content/uploads/2023/02/golang-gMW2A.jpg)


# 🚀 Go Finance

Bem-vindo(a). Este é o Go Finance!

O objetivo desta aplicação é simular um serviço financeiro, havendo clientes, transações e dados da conta.
Tbm são gerados dados analiticos para construção do dashboard!

# 🧠 Contexto

Tecnologias utilizadas neste projeto:
-  Golang (https://tip.golang.org/doc/go1.20)
-  SQLC (https://github.com/kyleconroy/sqlc)
-  Golang-migrate (https://github.com/golang-migrate/migrate)
-  GIN (https://github.com/gin-gonic/gin)
-  Postgres (https://www.postgresql.org/)
-  Pgadmin (https://www.pgadmin.org/)
-  Docker (https://docs.docker.com/get-started/overview/)
-  Testify para os testes unitários (https://github.com/stretchr/testify)
-  Kubernetes (https://kubernetes.io/docs/concepts/overview/)
-  Pipeline End to End (https://gitlab.com/marceloeduardo244/go-finance/-/pipelines)
    - Na pipeline temos 4 jobs
        - run_migrations para validar se as migrations estão saudáveis e válidas
        - unit_test para rodar todos os testes unitários da aplicação
        - generate_binary para gerar o arquivo binario da app (.bin)
        - deploy_batch_docker para fazer o build e atualizar a imagem no docker-hub

## 📋 Instruções para subir a aplicação utilizando Docker-compose

É necessário ter o Docker e Docker-compose instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- docker-compose up -d
- Vc terá 3 novos container rodando na sua maquina (https://gitlab.com/marceloeduardo244/go-finance/-/blob/a5117e501403c52821e481e93580a1e747cd3c3d/docs/docker-compose-running-containers.jpg)
- A API rodando por padrão na porta localhost:8080, do Postgres na porta localhost:5432 e o pgadmin na porta localhost:80.
- Abaixo nesta doc tem a documentação das rotas.

## 📋 Instruções para subir a aplicação utilizando o Kubernetes

É necessário ter o Docker e Docker-compose e Kubernets instalado e rodando na sua maquina!

- Faça clone do projeto
- Na raiz do projeto execute os comandos abaixo:
- docker-compose pull
- cd devops/kubernetes
- kubectl apply -f ./golang-go-finance
- Vc terá 6 novos container rodando na sua maquina (https://gitlab.com/marceloeduardo244/go-finance/-/blob/main/docs/kubernetes-running-containers.jpg)
- A API rodando por padrão na porta localhost:30002, o Postgres poderá ser acessado via pgadmin na porta localhost:30003
- Abaixo nesta doc tem a documentação das rotas.

## ✔️ Rotas da API

Alem da breve descrição abaixo sobre as rotas tbm há um link no fim para download da collection que poderá ser importada no seu postman ou insomnia:

- GET    /server/isAlive
- POST   /user
- GET    /user/:username
- GET    /user/id/:id
- PUT    /user/:id
- DELETE /user/id/:id
- POST   /category
- GET    /category/id/:id
- GET    /category/all/:user_id/:type
- DELETE /category/:id
- PUT    /category/:id
- POST   /account
- GET    /account/id/:id
- GET    /account/all/:user_id/:type
- GET    /account/graph/:user_id/:type
- GET    /account/reports/:user_id/:type
- DELETE /account/:id
- PUT    /account/:id
- POST   /login

- link para download da collection feita no postman que pode apox feito o download pode ser importada no postman ou insomnia (https://gitlab.com/marceloeduardo244/go-finance/-/blob/main/docs/GOLANG-GO_FINANCE.postman_collection.json) 

# Frontend da Aplicação
- https://gitlab.com/marceloeduardo244/go-finance-web

# Integração completa da aplicação
- https://gitlab.com/marceloeduardo244/go-finance-integrado

Made with 💜 at OliveiraDev
