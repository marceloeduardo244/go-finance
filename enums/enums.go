package enums

type DefaultMessages string

const (
	Unathorized DefaultMessages = "Username or password incorrect."
)

// EnumIndex
func (msg DefaultMessages) EnumIndex() string {
	return string(msg)
}
